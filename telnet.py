from socket import *
import struct
from threading import Thread

DOTS = " ..."
PORT = 8820

USER1 = {"username": "User1", "password": "pass", "email": "me@mail.com"}
USER2 = {"username": "User2", "password": "pass2", "email": "me2@mail.com"}

NAME = "name"


def connect_to_server(user):
    print(user["username"] + " - Connecting socket" + DOTS)
    soc_address = ("127.0.0.1", PORT)
    user["soc"] = socket(AF_INET, SOCK_STREAM)

    while True:
        try:
            user["soc"].connect(soc_address)
            break
        except Exception:
            continue
    print("Connected", "\n")


def talk_with_server(code, msg, user, addition=""):

    # Analyzing data
    format_ = '=bi%ds' % len(msg)
    packed_data = struct.pack(format_, code, len(msg), msg)
    ans = ""

    # Connecting to port and talking with server
    try:
        user["soc"].sendall(packed_data)
        if code is not 3 or 0:
            ans = user["soc"].recv(2048).decode()
            print(addition + "response is: " + ans)
    except Exception as e:
        print(e)
        exit()
    print()
    return ans


def log_in(user):
    print(user["username"] + " - Login in" + DOTS)
    talk_with_server(1, b'{"username": "' + user["username"].encode() +
                     b'","password": "' + user["password"].encode() + b'"}', user)


def signup(user):
    print(user["username"] + " - signing up" + DOTS)
    talk_with_server(2, b'{"username": "' + user["username"].encode() +
                     b'", "password": "' + user["password"].encode() +
                     b'", "email": "' + user["email"].encode() + b'"}', user)


def sign_out(user):
    print(user["username"] + " - leaving" + DOTS)
    if talk_with_server(3, b'', user) is "":
        print("request response: user left successfully")


def get_rooms(user):
    print(user["username"] + " - Getting rooms" + DOTS)
    talk_with_server(4, b'{"username": "' + user["username"].encode() +
                     b'","password": "' + user["password"].encode() + b'"}', user)


def create_room(user):
    print(user["username"] + " - Creating a room" + DOTS)
    talk_with_server(7, b'{"roomName": "BestRoom", "maxUsers": 4, "questionCount": 5, "answerTimeout" : 5}', user)


def join_room(user):
    print(user["username"] + " - join room" + DOTS)
    talk_with_server(6, b'{"roomid": 1}', user)
    t = Thread(target=wait_for_room_response, args=(user,))
    t.start()
    return t


def get_room_state(user):
    print(user["username"] + " - Getting room response" + DOTS)
    talk_with_server(11, b'{}', user)


def wait_for_room_response(user):
    addition = user["username"] + " - Getting room response" + DOTS
    while True:
        ans = talk_with_server(11, b'{}', user, addition + "\n")

        if ":(" in ans:
            break
        # time.sleep(.600)


def close_room(user):
    print(user["username"] + " - closing room" + DOTS)
    talk_with_server(12, b'{}', user)


def get_high_score(user):
    print(user["username"] + " - getting high score" + DOTS)
    talk_with_server(8, b'{}', user)


def get_players_in_room(user):
    print(user["username"] + " - Getting players in room" + DOTS)
    talk_with_server(5, b'{"roomid": 1}', user)


def main():
    """ The main"""
    print()
    connect_to_server(USER1)
    log_in(USER1)
    create_room(USER1)

    connect_to_server(USER2)
    log_in(USER2)
    get_rooms(USER2)
    get_players_in_room(USER2)
    get_high_score(USER2)
    t = join_room(USER2)

    # get_room_state(USER2)

    close_room(USER1)
    t.join()
    # get_room_state(USER2)
    sign_out(USER2)


if __name__ == "__main__":
    main()


