#pragma once
#include "RoomMemberRequestHandler.h"

class RoomAdminRequestHandler : public RoomMemberRequestHandler
{
private:
	GameManager* _gameManager;

	RequestResult closeRoom(Request);
	RequestResult startGame(Request);

public:
	RoomAdminRequestHandler(RequestHandlerFactory* fact, RoomManager* mang, LoggedUser user, int roomId);
	
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
};
