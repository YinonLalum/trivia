#pragma once

#include "iostream"

class SQLException : public std::exception
{
private:
	string _what;

public:
	SQLException(const std::string& message) : _what(message) { };
	SQLException() : _what("Error executing SQL command!") { };
	SQLException(unsigned int line) : _what("Error executing SQL command at line: ")
	{
		_what.append(to_string(line));
	}

	const char* what() const
	{
		return _what.c_str();
	}
};