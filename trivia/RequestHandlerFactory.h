#pragma once

#include "LoginManager.h"
#include "RoomManager.h"
#include "HighscoreTable.h"
#include "GameManager.h"

extern class LoginRequestHandler;
extern class MenuRequestHandler;
extern class RoomAdminRequestHandler;
extern class RoomMemberRequestHandler;
extern class GameRequestHandler;

class RequestHandlerFactory
{
private:
	LoginManager _loginManager;
	RoomManager _roomManager;
	HighscoreTable _highscoreTable;
	GameManager _gameManager;

public:
	RequestHandlerFactory(IDataBase* dataBase);
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	RoomMemberRequestHandler* createRoomAdminRequestHandler(LoggedUser user, int roomId);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, int roomId);
	GameRequestHandler* createGameRequestHandler(LoggedUser user);
	HighscoreTable getHighscoreTable() { return this->_highscoreTable; };
};