#include "RoomAdminRequestHandler.h"

RequestResult RoomAdminRequestHandler::closeRoom(Request req)
{
	CloseRoomResponse resp{ 1 };

	try
	{
		_room->getLock()->lock();
		_roomManager->deleteRoom(_room->getData().id);
		//_room->getLock()->unlock();
	}
	catch (const std::exception&)
	{
		resp.status = 0;
	}
	
	return RequestResult{	
		JsonResponsePacketSerializer::serializeResponse(resp),
		(IRequestHandler*)_handlerFactory->createMenuRequestHandler(_user)
	};
}

RequestResult RoomAdminRequestHandler::startGame(Request)
{
	StartGameResponse res{ 1 };
	try
	{
		_gameManager->createGame(*_room);
		_room->setActivated();
	}
	catch (const std::exception&)
	{
		res.status = 0;
	}

	return RequestResult{
		JsonResponsePacketSerializer::serializeResponse(res), 
		(IRequestHandler*)_handlerFactory->createGameRequestHandler(_user)
	};
}

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory* fact, RoomManager* mang, LoggedUser user, int roomId)
{
	_handlerFactory = fact;
	_user = user;
	_roomManager = mang;
	_room = _roomManager->getRoomByReferance(roomId);
}

bool RoomAdminRequestHandler::isRequestRelevant(Request req)
{
	return RoomMemberRequestHandler::isRequestRelevant(req)
		|| req.id == requestID::closeRoom
		|| req.id == requestID::startGame;
}

RequestResult RoomAdminRequestHandler::handleRequest(Request req)
{
	return (req.id == requestID::closeRoom || req.id == requestID::leaveRoom) ? closeRoom(req) : 
		   req.id == requestID::getRoomState ? getRoomState(req) : startGame(req);
}
