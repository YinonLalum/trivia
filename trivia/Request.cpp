#pragma once

#include "Request.h"
#include "InvalidJsonException.h"
using namespace nlohmann;

unsigned int JsonRequestPacketDeserializer::deserializeCode(buffer buffer)
{
	unsigned int num = buffer.getbuffer()[0];
	return num;
}

unsigned int JsonRequestPacketDeserializer::deserializeLength(buffer buffer)
{
	int* num = (int*)buffer.getbuffer();
	return *num;
}

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(buffer buf)
{
	LoginRequest req;
	json data = json::parse(buf.getbuffer());
	try
	{
		req.username = data["username"].get<string>();
		req.password = data["password"].get<string>();
	}
	catch (...)
	{
		throw InvalidJsonException("One or more arguments were invalid!");
	}
	return req;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(buffer buffer)
{
	SignupRequest req;
	json data = json::parse(buffer.getbuffer());
	try
	{
		req.username = data["username"].get<string>();
		req.password = data["password"].get<string>();
		req.email = data["email"].get<string>();
	}
	catch (const std::exception&)
	{
		throw InvalidJsonException("One or more arguments were invalid!");
	}
	return req;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(buffer buffer)
{
	GetPlayersInRoomRequest req;
	json data = json::parse(buffer.getbuffer());
	try
	{
		req.roomId = data["roomid"].get<int>();
	}
	catch (const std::exception&)
	{
		throw InvalidJsonException("One or more arguments were invalid!");
	}
	return req;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(buffer buffer)
{
	JoinRoomRequest req;
	json data = json::parse(buffer.getbuffer());
	try
	{
		req.roomId = data["roomid"].get<int>();
	}
	catch (const std::exception&)
	{
		throw InvalidJsonException("One or more arguments were invalid!");
	}
	return req;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(buffer buffer)
{
	CreateRoomRequest req;
	json data = json::parse(buffer.getbuffer());
	try
	{
		req.roomName = data["roomName"].get<string>();
		req.maxUsers = data["maxUsers"].get<unsigned int>();
		req.questionCount = data["questionCount"].get<unsigned int>();
		req.answerTimeout = data["answerTimeout"].get<unsigned int>();
	}
	catch (const std::exception&)
	{
		throw InvalidJsonException("One or more arguments were invalid!");
	}
	return req;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(buffer buffer)
{
	return SubmitAnswerRequest();
}
