#pragma once

#include "Resurces.h"
#include "sqlite3.h"
#include "IDataBase.h"
#include <sstream>


class SqliteDataBase : public IDataBase
{
private:
	sqlite3* db;

public:
	SqliteDataBase(string path);
	~SqliteDataBase();

	// Getters
	map<LoggedUser, int> getHighscores();
	vector<Question> getQuestions(int num);

	//Setter
	void addUser(string username, string password, string email);
	
	// Queries
	bool doesUserExist(string s);
	bool canLogin(string name, string password);
};

namespace callbacks
{
	int hasMatches(void* data, int argc, char** argv, char** azColName);
	int highscore(void* data, int argc, char** argv, char** azColName);
	int questions(void* data, int argc, char** argv, char** azColName);
	int users(void* data, int argc, char** argv, char** azColName);
}