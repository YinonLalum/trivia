#pragma once

#include "RequestHandlerFactory.h"
#include "Resurces.h"
#include "buffer.hpp"
#include "Request.h"
#include "Response.h"

extern class RequestHandlerFactory;

typedef struct Request
{
	int id;
	time_t recivalTime;
	buffer buffer;
} Request;

typedef struct RequestResult RequestResult;

class IRequestHandler
{
protected:
	RequestHandlerFactory* _handlerFactory;

public:
	virtual bool isRequestRelevant(Request req) = 0;
	virtual RequestResult handleRequest(Request) = 0;
	virtual void suddenDC() = 0;

};

typedef struct RequestResult
{
	buffer response;
	IRequestHandler* newHandler;
} RequestResult;
