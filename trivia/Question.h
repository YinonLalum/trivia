#pragma once

#include "Resurces.h"

#define CORRRECT_ANSWER_INDEX 0

class Question
{
public:
	// Ctors
	Question(string question, vector<string> possibleAnswers);
	Question() {};

	// Getters
	string getQuestion();
	vector<string> getPossibleAnswers();
	string getCorrectAnswer();

	void addPossibleAns(string ans);

private:
	string _question;
	vector<string> _possibleAnswers;
};