#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"

class LoginRequestHandler : public IRequestHandler
{
private:
	LoginManager* _loginManager;

private:
	RequestResult login(Request);
	RequestResult signup(Request);

public:
	LoginRequestHandler(RequestHandlerFactory* f, LoginManager* mang);
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
	void suddenDC();

};
