#pragma once

#include "Resurces.h"
#include "IDataBase.h"
#include "Game.h"
#include "Room.h"

class GameManager
{
private:
	IDataBase* _dataBase;
	vector<Game> _games;

public:
	GameManager(IDataBase* db);
	Game createGame(Room);
	void deleteGame(/*???*/);
};