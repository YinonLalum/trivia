#include "RoomManager.h"

//lock_guard<mutex>* _lockGuard;
unique_lock<mutex>* _lock;

unsigned int RoomManager::getNewID()
{
	unsigned int lastID = 0;

	for (std::pair<unsigned int, Room*> room : _rooms)
	{
		if (room.first > lastID)
		{
			lastID = room.first;
		}
	}

	return lastID + 1;
}

RoomManager::RoomManager()
{
	_lock = new unique_lock<mutex>(*(new mutex()));
	_lock->unlock();
	/*_lockGuard = new lock_guard<mutex>(*_lock);
	_lock->unlock();*/
}

RoomManager::~RoomManager()
{
}

int RoomManager::createRoom(CreateRoomRequest req)
{
	_lock->lock();
	
	int id = getNewID();
	Room* newRoom = new Room(RoomData{ (unsigned int)id, req.roomName, req.maxUsers, req.answerTimeout, NOT_ACTIVE, req.questionCount });
	_rooms.insert({ newRoom->getData().id, newRoom });
	
	_lock->unlock();

	return id;
}

void RoomManager::deleteRoom(int id)
{
	if ( _rooms.at(id)->getData().isActive )
	{
		yeet exception("Room already active!!!");
	}
	
	try
	{
		_lock->lock();
		delete _rooms.at(id);
		_rooms.erase(_rooms.find(id));
		_lock->unlock();
	}
	catch (...) {}
}

unsigned int RoomManager::getRoomState(int id)
{
	return getRoomByReferance(id)->getData().isActive;
}

Room* RoomManager::getRoomByReferance(int id)
{
	Room* room = nullptr;

	_lock->lock();
	if (doesRoomExist(id))
	{
		room = this->_rooms.at(id);
	}
	_lock->unlock();

	return room;
}

vector<Room*> RoomManager::getRooms()
{
	vector<Room*> res;

	_lock->lock();
	for (pair<int, Room*> it : _rooms)
	{
		res.push_back(it.second);
	}
	_lock->unlock();

	return res;
}

Room RoomManager::getRoom(int id)
{
	return *getRoomByReferance(id);
}

vector<RoomData> RoomManager::getRoomsData()
{
	vector<RoomData> res;

	_lock->lock();
	for (pair<int, Room*> it : _rooms)
	{
		res.push_back(it.second->getData());
	}
	_lock->unlock();

	return res;
}

bool RoomManager::doesRoomExist(int id)
{
	return _rooms.find(id) != _rooms.end();


	/*for (pair<unsigned int, Room*> room : _rooms)
	{
		if (room.first == id) 
		{
			return true;
		}
	}
	return false;*/
}
