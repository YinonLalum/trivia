#include "Room.h"
#include <iostream>
#include <list>

using std::list;

Room::Room(RoomData data) :
	_roomData(data)
{	
	_lock = new mutex();
	_lockGuard = new lock_guard<mutex>(*_lock);
	_lock->unlock();
}

Room::~Room()
{
	//delete _lockGuard;
}

void Room::setActivated()
{
	_roomData.isActive = 1;
}

void Room::addUser(LoggedUser user)
{
	_users.push_back(user);
}

mutex* Room::getLock()
{
	return _lock;
}

RoomData Room::getData()
{
	return RoomData{ _roomData.id, _roomData.name, _roomData.maxPlayers, _roomData.timePerQuestion, _roomData.isActive, _roomData.questionCount };
}

void Room::removeUser(LoggedUser user)
{
	_users.erase(find<vector<LoggedUser>::iterator>(_users.begin(), _users.end(), user));
}

void Room::lock()
{
	_lock->lock();
}

void Room::unlock()
{
	_lock->unlock();
}

vector<LoggedUser> Room::getAllUsers()
{
	return _users;
}
