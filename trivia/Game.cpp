#include "Game.h"

Game::Game(Room room)
{
	// Initiating questions
	IDataBase* db = new SqliteDataBase("DataBase.db");
	_questions = db->getQuestions(room.getData().maxPlayers);

	// Initiating players
	for (LoggedUser user : room.getAllUsers())
	{
		_players.insert(pair<LoggedUser, GameData>(user, GameData{ _questions[0], 0, 0, 0 }));
	}
}
