#pragma once
#include <windows.h>
#include "Resurces.h"

typedef struct RoomData
{
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int isActive;
	unsigned int questionCount;
} RoomData;

class Room
{
private:
	lock_guard<mutex>* _lockGuard;
	mutex* _lock;
	RoomData _roomData;
	vector<LoggedUser> _users;

public:

	// Ctor + Dtor
	Room(RoomData);
	~Room();

	// Setters
	void setActivated();
	void addUser(LoggedUser);
	void removeUser(LoggedUser);
	void lock();
	void unlock();

	// Getters
	mutex* getLock();
	RoomData getData();
	vector<LoggedUser> getAllUsers();

};