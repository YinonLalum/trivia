#pragma once

#include "Resurces.h"
#include "GameManager.h"
#include "Game.h"
#include "IRequestHandler.h"

class RoomMemberRequestHandler : public IRequestHandler
{
protected:
	int _roomID;
	Room* _room;
	RoomManager* _roomManager;
	LoggedUser _user;

protected:
	RequestResult leaveRoom(Request);
	RequestResult getRoomState(Request);

public:
	RoomMemberRequestHandler() = default;
	RoomMemberRequestHandler(RequestHandlerFactory* fact, RoomManager* mang, LoggedUser user, int roomId);

	virtual bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
	void suddenDC();
};
