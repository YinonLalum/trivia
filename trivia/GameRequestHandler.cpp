#include "GameRequestHandler.h"

RequestResult GameRequestHandler::getQuestion(Request)
{
	return RequestResult();
}

RequestResult GameRequestHandler::submitAnswer(Request)
{
	return RequestResult();
}

RequestResult GameRequestHandler::getGameResults(Request)
{
	return RequestResult();
}

RequestResult GameRequestHandler::leaveGame(Request)
{
	return RequestResult();
}

GameRequestHandler::GameRequestHandler(RequestHandlerFactory* fact, GameManager* mang, LoggedUser user)
{
	_handlerFactory = fact;
	_gameManager = mang;
	_user = user;
	//_game = new Game(&_room);
}

bool GameRequestHandler::isRequestRelevant(Request)
{
	return false;
}

void GameRequestHandler::suddenDC()
{
}

RequestResult GameRequestHandler::handleRequest(Request)
{
	return RequestResult();
}
