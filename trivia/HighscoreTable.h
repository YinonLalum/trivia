#pragma once

#include "IDataBase.h"

class HighscoreTable
{
private:
	IDataBase*_database;

public:
	HighscoreTable(IDataBase* db);
	map<LoggedUser, int> getHighscores() { return _database->getHighscores(); };
};