#pragma once

#include <map>
#include "GameManager.h"
#include "IRequestHandler.h"

class MenuRequestHandler : public IRequestHandler
{
private:
	RequestResult signout(Request);
	RequestResult getRooms(Request);
	RequestResult getPlayersInRoom(Request);
	RequestResult getHighscores(Request);
	RequestResult joinRoom(Request);
	RequestResult createRoom(Request);

private:
	using handler_func_t = RequestResult (MenuRequestHandler::*)(Request);
	HighscoreTable* _highscoreTable;
	LoggedUser _user;
	RoomManager* _roomManager;
	LoginManager* _loginManager;
	static const std::map<requestID, handler_func_t> m_commands;

public:
	MenuRequestHandler(RequestHandlerFactory* f,RoomManager* mang, HighscoreTable* table,LoginManager* login, LoggedUser user);
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
	void suddenDC();
};

