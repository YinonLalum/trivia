#include "Response.h"
#include <sstream>

buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse resp)
{
	return buffer(resp.message);
}

buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse resp)
{
	if (resp.status)
		return buffer("User logged in successfully! :)");
	else
		return buffer("User failed to log in! :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse resp)
{
	if (resp.status)
		return buffer("User signed up succesfully! :)");
	else
		return buffer("User failed to sign up! :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse resp)
{
	return buffer();
}

buffer JsonResponsePacketSerializer::serializeResponse(GetRoomResponse resp)
{
	stringstream s;
	s << "{";
	for (Room room : resp.rooms)
	{
		s << "(" << room.getData().name << ", " << room.getData().id << ")";
	}
	s << "}";
	return buffer(s.str());
}

buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse resp)
{
	stringstream s;
	s << "{";
	for (string player : resp.players)
	{
		s << "(" << player << ")";
	}
	s << "}";
	return buffer(s.str());	
}

buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse resp)
{
	if (resp.status)
		return buffer("Joined room succesfully! :)");
	else
		return buffer("Failed to create room :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse resp)
{
	if (resp.status)
		return buffer("A room created a succesfully! :)");
	else
		return buffer("Failed to create a room :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(HighscoreResponse resp)
{
	stringstream s;

	if (resp.status)
	{
		s << "{";
		for (Highscore hiscore : resp.highscores)
		{
			s << "(" << hiscore.score.first << ", " << hiscore.score.second << ")";
		}
		s << "}";
		return buffer(s.str());
	}
	else
		return buffer("Failed to achive highscore :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse resp)
{
	if (resp.status)
		return buffer("Room deleted succesfully! :)");
	else
		return buffer("Failed to delete room :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse resp)
{
	return buffer();
}

buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse resp)
{
	stringstream s;

	if (resp.status)
	{
		s << "{(";
		for (string player : resp.players)
		{
			s << player;
			if (player != *(resp.players.end() - 1))
			{
				s << ", ";
			}
		}
		s << "), " << resp.ansTimeOut << ", " << resp.hasGameBegun << ", " << resp.questionCount;
		s << "}";
		return buffer(s.str());
	}
	else
		return buffer("Room deosn't exist or had been deleted :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse resp)
{
	if (resp.status)
		return buffer("Leave room succesfully! :)");
	else
		return buffer("Failed to leave room :(");
}

buffer JsonResponsePacketSerializer::serializeResponse(GetQuestionRespomse resp)
{
	return buffer();
}

buffer JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse resp)
{
	return buffer();
}

buffer JsonResponsePacketSerializer::serializeResponse(GetGameResultResponse resp)
{
	return buffer();
}
