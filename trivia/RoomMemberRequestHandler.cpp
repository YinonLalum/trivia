#include "RoomMemberRequestHandler.h"
#include "MenuRequestHandler.h"

RequestResult RoomMemberRequestHandler::leaveRoom(Request)
{
	LeaveRoomResponse resp = { 1 };

	try
	{
		_room->removeUser(_user);
	}
	catch (...)
	{
		resp.status = 0;
	}

	return RequestResult{
		JsonResponsePacketSerializer::serializeResponse(resp),
		(IRequestHandler*)_handlerFactory->createMenuRequestHandler(_user)
	};
}

RequestResult RoomMemberRequestHandler::getRoomState(Request)
{
	RequestResult res = {};
	GetRoomStateResponse resp = { 0, false, vector<string>(), 0, 0 };
	
	if (_roomManager->doesRoomExist(_roomID))
	{
		_room->lock();
		RoomData data = _room->getData();
		resp.players = _room->getAllUsers();
		_room->unlock();

		resp.ansTimeOut = data.timePerQuestion;
		resp.questionCount = data.questionCount;
		resp.hasGameBegun = (bool)(data.isActive);
		resp.status = 1;

		res.newHandler = (IRequestHandler*)_handlerFactory->createRoomMemberRequestHandler(_user, data.id);
	}
	else
	{
		res.newHandler = (IRequestHandler*)_handlerFactory->createMenuRequestHandler(_user);
	}

	res.response = JsonResponsePacketSerializer::serializeResponse(resp);

	return res;
}

RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory* fact, RoomManager* mang, LoggedUser user, int roomId)
{
	_user = user;
	_handlerFactory = fact;
	_roomManager = mang;
	_room = _roomManager->getRoomByReferance(roomId);
	_roomID = _room->getData().id;
}


bool RoomMemberRequestHandler::isRequestRelevant(Request req)
{
	return req.id == requestID::leaveRoom || req.id == requestID::getRoomState;
}

RequestResult RoomMemberRequestHandler::handleRequest(Request req)
{
	return req.id == requestID::leaveRoom ? leaveRoom(req) : getRoomState(req);
}

void RoomMemberRequestHandler::suddenDC()
{
	MenuRequestHandler* handl = (MenuRequestHandler*)_handlerFactory->createMenuRequestHandler(_user);
	handl->suddenDC();
	delete handl;
}
