#include "MenuRequestHandler.h"

RequestResult MenuRequestHandler::signout(Request)
{
	LogoutResponse res = { 1 };
	try
	{
		_loginManager->logout(_user);
	}
	catch (...)
	{
		res.status = 0;
	}
	return RequestResult{JsonResponsePacketSerializer::serializeResponse(res), nullptr};
}

RequestResult MenuRequestHandler::getRooms(Request)
{
	GetRoomResponse resp{ 1 };
	try
	{
		resp.rooms = _roomManager->getRoomsData();
	}
	catch (...)
	{
		resp.status = 0;
	}

	RequestResult result
	{
		JsonResponsePacketSerializer::serializeResponse(resp),
		_handlerFactory->createMenuRequestHandler(_user)
	};
	
	return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(Request request)
{
	GetPlayersInRoomRequest req = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request.buffer);
	GetPlayersInRoomResponse res{ _roomManager->getRoom(req.roomId).getAllUsers() };
	
	// Initiating the response
	RequestResult result
	{
		JsonResponsePacketSerializer::serializeResponse(res), 
		_handlerFactory->createMenuRequestHandler(_user)
	};
	
	return result;
}

RequestResult MenuRequestHandler::getHighscores(Request)
{
	HighscoreTable highscoreMap = this->_handlerFactory->getHighscoreTable();
	vector<std::pair<LoggedUser, int>> highscoreVec;
	HighscoreResponse res;

	// Making a vector of the scors
	for (pair<LoggedUser, int> score : highscoreMap.getHighscores())
	{
		res.highscores.push_back(Highscore{score});
	}
	res.status = 1;

	// Prepering the result
	RequestResult result
	{
		JsonResponsePacketSerializer::serializeResponse(res),
		_handlerFactory->createMenuRequestHandler(_user)
	};

	return result;
}

RequestResult MenuRequestHandler::joinRoom(Request request)
{
	JoinRoomRequest req = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request.buffer);
	JoinRoomResponse res{ 1 };
	RequestResult result;

	try
	{
		// In case of success the next handler is set to room member handler
		Room* room = _roomManager->getRoomByReferance(req.roomId);
		if (!room)
		{
			yeet exception("fuck :( !!!");
		}
		room->addUser(_user);
		result.newHandler = (IRequestHandler*)_handlerFactory->createRoomMemberRequestHandler(_user, req.roomId);
	}
	catch (...)
	{
		// In case of failier the hanlder is the same
		result.newHandler = (IRequestHandler*)_handlerFactory->createMenuRequestHandler(_user);
		res.status = 0;
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(res);

	return result;
}

RequestResult MenuRequestHandler::createRoom(Request request)
{
	int id = -1;
	CreateRoomResponse resp = { 1 };
	CreateRoomRequest req = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request.buffer);

	try
	{
		id = _roomManager->createRoom(req);
		_roomManager->getRoomByReferance(id)->addUser(_user);
	}
	catch (...)
	{
		resp.status = 0;
	}

	// Orgenizing the respoonse
	RequestResult res
	{
		JsonResponsePacketSerializer::serializeResponse(resp),
		(IRequestHandler*)_handlerFactory->createRoomAdminRequestHandler(_user, id)
	};

	return res;
}

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* f, RoomManager* mang, HighscoreTable* table, LoginManager* login, LoggedUser user)
{
	_loginManager = login;
	_handlerFactory = f;
	_roomManager = mang;
	_highscoreTable = table;
	_user = user;
}

bool MenuRequestHandler::isRequestRelevant(Request request)
{
	return
		request.id == requestID::createRoom			||
		request.id == requestID::leave				||
		request.id == requestID::joinRoom			||
		request.id == requestID::signout			||
		request.id == requestID::getPlayersInRoom	||
		request.id == requestID::getRooms			||
		request.id == requestID::getMyStatus		||
		request.id == requestID::getHighScores;
}

void MenuRequestHandler::suddenDC()
{
	this->signout(Request());
}

RequestResult MenuRequestHandler::handleRequest(Request req)
{
	MenuRequestHandler::handler_func_t handler = m_commands.at((requestID)req.id);
	return (this->*handler)(req);
}

const std::map<requestID, MenuRequestHandler::handler_func_t> MenuRequestHandler::m_commands =
{
	std::pair<requestID, MenuRequestHandler::handler_func_t>
	{ requestID::signout, &MenuRequestHandler::signout },
	{ requestID::leave ,& MenuRequestHandler::signout },
	{ requestID::getRooms, &MenuRequestHandler::getRooms },
	{ requestID::getPlayersInRoom, &MenuRequestHandler::getPlayersInRoom },
	{ requestID::getHighScores, &MenuRequestHandler::getHighscores },
	{ requestID::joinRoom, &MenuRequestHandler::joinRoom },
	{ requestID::createRoom, &MenuRequestHandler::createRoom }
};