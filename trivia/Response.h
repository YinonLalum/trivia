#pragma once

#include "Resurces.h"
#include "Room.h"
#include "json.hpp"
#include "buffer.hpp"

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;


typedef struct LogoutResponse
{
	unsigned int status;
}LogoutResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CloseRoomResponse
{
	unsigned int status;
}CloseRoomResponse;

typedef struct GetQuestionRespomse
{
	unsigned int status;
	string question;
	map<unsigned int, string> answer;
}GetQuestionRespomse;

typedef struct SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct GetRoomResponse
{
	unsigned int status;
	vector<RoomData> rooms;
}GetRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;
}CreateRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
}StartGameResponse;

typedef struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
}SubmitAnswerResponse;

typedef struct GetPlayersInRoomResponse
{
	vector<string> players;
}GetPlayersInRoomResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	vector<string> players;
	unsigned int questionCount;
	unsigned int ansTimeOut;
}GetRoomStateResponse;


typedef struct PlayerResult
{
	string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
}PlayerResult;

typedef struct GetGameResultResponse
{
	unsigned int status;
	vector<PlayerResult> results;
}GetGameResultResponse;

typedef struct Highscore
{
	pair<LoggedUser, unsigned int> score;
}Highscore;

typedef struct HighscoreResponse
{
	unsigned int status;
	vector<Highscore> highscores;
}HighscoreResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
}LeaveRoomResponse;

typedef struct ErrorResponse
{
	string message;
}ErrorResponse;


namespace JsonResponsePacketSerializer
{
	buffer serializeResponse(ErrorResponse resp);
	buffer serializeResponse(LoginResponse resp);
	buffer serializeResponse(SignupResponse resp);
	buffer serializeResponse(LogoutResponse resp);
	buffer serializeResponse(GetRoomResponse resp);
	buffer serializeResponse(GetPlayersInRoomResponse resp);
	buffer serializeResponse(JoinRoomResponse resp);
	buffer serializeResponse(CreateRoomResponse resp);
	buffer serializeResponse(HighscoreResponse resp);
	buffer serializeResponse(CloseRoomResponse resp);
	buffer serializeResponse(StartGameResponse resp);
	buffer serializeResponse(GetRoomStateResponse resp);
	buffer serializeResponse(LeaveRoomResponse resp);
	buffer serializeResponse(GetQuestionRespomse resp);
	buffer serializeResponse(SubmitAnswerResponse resp);
	buffer serializeResponse(GetGameResultResponse resp);
	//char* serializeResultsResponse(GetResultsResponse resp); //what????
}