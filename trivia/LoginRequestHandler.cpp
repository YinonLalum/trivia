#include "LoginRequestHandler.h"

RequestResult LoginRequestHandler::login(Request req)
{
	RequestResult res;
	LoginResponse resp;
	LoginRequest sReq = JsonRequestPacketDeserializer::deserializeLoginRequest(req.buffer);
	
	// Adding the user to database in case the connection secceed
	bool success = _loginManager->login(sReq.username, sReq.password);
	resp.status = (int)success;

	// Adjusting the next handler
	if (success)
		res.newHandler = (IRequestHandler*)this->_handlerFactory->createMenuRequestHandler(sReq.username);
	else
		res.newHandler = (IRequestHandler*)this->_handlerFactory->createLoginRequestHandler();

	res.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return res;
}

RequestResult LoginRequestHandler::signup(Request req)
{
	RequestResult res;
	SignupResponse resp;
	SignupRequest sReq = JsonRequestPacketDeserializer::deserializeSignupRequest(req.buffer);
	resp.status = 1;

	// Adding the user to the database
	try
	{
		_loginManager->signup(sReq.username, sReq.password, sReq.email);
		res.newHandler = (IRequestHandler*)this->_handlerFactory->createMenuRequestHandler(sReq.username);
	}
	catch (const std::exception&)
	{
		resp.status = 0;
		res.newHandler = (IRequestHandler*)this->_handlerFactory->createLoginRequestHandler();
	}
	res.response = JsonResponsePacketSerializer::serializeResponse(resp);
	
	return res;
}

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* f, LoginManager* mang)
{
	_loginManager = mang;
	_handlerFactory = f;
}

bool LoginRequestHandler::isRequestRelevant(Request req)
{
	return req.id == requestID::login || req.id == requestID::signup;
}

RequestResult LoginRequestHandler::handleRequest(Request req)
{
	return req.id == requestID::login ? login(req) : signup(req);
}

void LoginRequestHandler::suddenDC()
{
	//if it happens here, no additional action is required since this is the login handler
}
