#pragma once
#include "Question.h"
#include "SQLException.h"

class IDataBase
{

public:
	virtual map<LoggedUser, int> getHighscores() = 0;
	virtual bool doesUserExist(string) = 0;
	virtual vector<Question> getQuestions(int) = 0;
	virtual void addUser(string username, string password,string email) = 0;
	virtual bool canLogin(string name, string password) = 0;
};