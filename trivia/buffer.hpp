#pragma once
#include "Resurces.h"

class buffer
{
private:
	char* data;
	int size;

public:
	buffer(size_t size)
	{
		data = new char[size + sizeof(int)];
		data[size] = 0;
		this->size = size;
	};

	buffer(char* oldData, size_t size) : buffer(size)
	{
		for (int i = 0; i < size; i++)
		{
			data[i] = oldData[i];
		}
	};
	buffer()
	{
		data = new char[sizeof(int)];
		data[0] = 0;
		size = 0;
	};
	buffer(const buffer& buffer) : buffer(buffer.data, buffer.size) {};
	buffer(string s) : buffer(s.length())
	{
		strcpy((char*)data, s.c_str());
	}
	~buffer()
	{
		delete[] data;
	};

	buffer& operator=(const buffer& other)
	{
		data = new char[other.size + sizeof(int)];
		data[other.size] = 0;
		this->size = other.size;
		for (int i = 0; i < size; i++)
		{
			data[i] = other.data[i];
		}
		return *this;
	}

	int getSize()
	{
		return size;
	};

	char* getbuffer()
	{
		return data;
	};


};