
#include "Resurces.h"
#include "Communicator.h"
#include "SqliteDataBase.h"
#include <thread>
#include <conio.h>

#define SHUT_DOWN '\r'
#define PORT 8820

void toExit();

int main()
{
	std::thread(toExit).detach();
	IDataBase* db = new SqliteDataBase("DataBase.db");
	RequestHandlerFactory handlerFactory(db);
	Communicator communicator(&handlerFactory);

	try
	{
		cout << endl << "Press Enter to shut down" << endl << endl;
		communicator.bindAndListen(PORT);
		cout << "please" << endl;
	}
	catch (const std::exception& e)
	{
		std::cerr << "Error was: " << e.what() << endl;
	}
	system("pause");
	return 0;
}

void toExit()
{
	while (true)
	{
		if (_getch() == SHUT_DOWN)
		{
			exit(1);
		}
	}
}