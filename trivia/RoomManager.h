#pragma once

#include "Resurces.h"
#include "request.h"
#include "Game.h"
#include "Room.h"

#define NOT_ACTIVE 0

class RoomManager
{
private: //map is - key: int id, value: Room
	map<unsigned int, Room*> _rooms;
	unsigned int getNewID(); 

public:
	// Ctor + Dtor
	RoomManager();
	~RoomManager();

	// Setters
	int createRoom(CreateRoomRequest);
	void deleteRoom(int id);

	// Getters
	unsigned int getRoomState(int id);
	Room* getRoomByReferance(int id);
	vector<Room*> getRooms();
	Room getRoom(int id);
	vector<RoomData> getRoomsData();
	bool doesRoomExist(int id);
};