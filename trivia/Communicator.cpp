#pragma once

#include "Communicator.h"
#include "Request.h"
#include "Response.h"
#include <thread>
#include "LoginRequestHandler.h"
#include "InvalidJsonException.h"

#define RECIEVE(soc, buf, size) \
if (recv(soc, buf, size, 0) <= 0) \
{ \
	closesocket(soc); \
	break; \
}

void Communicator::startThreadForNewClient(SOCKET soc)
{
	std::thread(&Communicator::handleRequest,this,soc).detach();
}

Communicator::Communicator(RequestHandlerFactory* handlerFactory) : _handlerFactory(handlerFactory)
{
	WSADATA wsa_data = { };
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw std::exception("WSAStartup Failed");

	serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(serverSocket);
	}
	catch (...) {}

	try
	{
		WSACleanup();
	}
	catch (...) {}
}

void Communicator::bindAndListen(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		yeet std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(serverSocket, SOMAXCONN) == SOCKET_ERROR)
		yeet std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;
	// the main thread is only accepting clients 
	// and add then to the list of handlers
	cout << "Waiting for client connection request" << endl;

	while (true)
	{
		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			yeet std::exception(__FUNCTION__);

		cout << "Client accepted. Server and client can speak" << endl;

		// the function that handle the conversation with the client
		_clients[client_socket] = _handlerFactory->createLoginRequestHandler();
		startThreadForNewClient(client_socket);
	}
}

void Communicator::handleRequest(SOCKET soc)
{
	RequestResult res;
	Request req;
	unsigned int size = 0;
	buffer length(MSG_LENGTH_SIZE);
	buffer id(CODE_SIZE);
	
	try
	{
		while (true)
		{
			req.recivalTime = std::time(0);

			//Getting the code id
			RECIEVE(soc, id.getbuffer(), CODE_SIZE);
			req.id = JsonRequestPacketDeserializer::deserializeCode(id);

			// Getting data length
			RECIEVE(soc, length.getbuffer(), MSG_LENGTH_SIZE);
			size = JsonRequestPacketDeserializer::deserializeLength(length);

			// Getting the data
			buffer buf(size);
			RECIEVE(soc, buf.getbuffer(), size);
			req.buffer = buf;

			try
			{
				// Handling request only if need
				if (!_clients[soc]->isRequestRelevant(req))
				{
					yeet std::exception("request was irrelavent! :(");
				}

				res = _clients[soc]->handleRequest(req);
				delete _clients[soc];
			}
			catch( const std::exception& e)
			{
				// Responding about the error to client
				cerr << e.what() << endl;
				ErrorResponse resp = { e.what() };
				res.response = JsonResponsePacketSerializer::serializeResponse(resp);
				res.newHandler = _clients[soc];

			}
			// Updating handler and sending response
			_clients[soc] = res.newHandler;
			send(soc, res.response.getbuffer(), res.response.getSize(), 0);
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << endl;
	}
	//_clients[soc]->suddenDC();
	cout << "User has disconnected!" << endl;
}
