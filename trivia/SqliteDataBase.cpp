#pragma once

#include "SqliteDataBase.h"
#include <io.h>

#define SQL_CALL(expr) if (expr != SQLITE_OK) yeet SQLException(__LINE__);
#define SQL_EXEC(s) SQL_CALL(sqlite3_exec(db, s.c_str(), nullptr, nullptr, nullptr));
#define SQL_EXEC_SS(ss) SQL_EXEC(ss.str());

SqliteDataBase::SqliteDataBase(string path)
{
	bool exists = !_access(path.c_str(), 0);
	SQL_CALL(sqlite3_open(path.c_str(), &db));

	if (!exists)
	{
		string users("CREATE TABLE users (\
					 id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
					 score INTEGER, \
					 name TEXT NOT NULL, \
				 	 password TEXT NOT NULL, \
					 email TEXT NOT NULL); ");

		string questions("CREATE TABLE questions (\
						  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
						  ans TEXT NOT NULL, \
						  wrong1 TEXT NOT NULL, \
						  wrong2 TEXT NOT NULL, \
						  wrong3 TEXT NOT NULL); ");

		SQL_EXEC(users);
		SQL_EXEC(questions);
	}

}

SqliteDataBase::~SqliteDataBase()
{
	sqlite3_close(db);
}

void SqliteDataBase::addUser(string username, string password,string email)
{
	stringstream ss;
	ss << "insert into users(score, name, password, email) values(0, \"" << username << "\", \"" << password << "\", \"" << email << "\");";
	SQL_EXEC_SS(ss);
}

map<LoggedUser, int> SqliteDataBase::getHighscores()
{
	stringstream ss;
	ss << "select score, name from users;";
	map<LoggedUser, int> highscoreMap;
	SQL_CALL(sqlite3_exec(db, ss.str().c_str(), callbacks::highscore, &highscoreMap, 0));
	return highscoreMap;
}

bool SqliteDataBase::doesUserExist(string s)
{
	bool ret = false;
	stringstream ss;
	ss << "select * from users where name='" << s << "';";
	SQL_CALL(sqlite3_exec(db, ss.str().c_str(), callbacks::hasMatches, &ret, 0));
	return ret;
}

bool SqliteDataBase::canLogin(string name, string password)
{

	/*stringstream tryy;
	tryy << "select * from users;";
	cout << endl <<  endl << "All existing users in the system" << endl;
	SQL_CALL(sqlite3_exec(db, tryy.str().c_str(), callbacks::users, nullptr, nullptr));
	cout << endl;*/

	bool ret = false;
	stringstream ss;
	ss << "select * from users where name = \"" << name << "\" and password = \"" << password << "\";";
	SQL_CALL(sqlite3_exec(db, ss.str().c_str(), callbacks::hasMatches, &ret, 0));
	return ret;
}

vector<Question> SqliteDataBase::getQuestions(int num)
{
	vector<Question> questions;
	stringstream ss;
	ss << "select * from questions limit " << num << ";";
	SQL_CALL(sqlite3_exec(db, ss.str().c_str(), callbacks::questions, &questions, 0));
	return questions;
}

int callbacks::hasMatches(void* data, int argc, char** argv, char** azColName)
{
	bool* res = (bool*)data;
	*res = argc > 0;
	return 0;
}

int callbacks::highscore(void* data, int argc, char** argv, char** azColName)
{
	pair<LoggedUser, int> score;

	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "name")
		{
			score.first = argv[i];
		}
		else if (string(azColName[i]) == "score")
		{
			score.second = std::atoi(argv[i]);
		}
	}

	((map<LoggedUser, int>*)data)->insert(score);

	return 0;
}

int callbacks::questions(void* data, int argc, char** argv, char** azColName)
{
	Question question;

	for (int i = 0; i < argc; i++)
	{
		// Making sure that the right ans get in first
		if (string(azColName[i]) == "ans")
		{
			question.addPossibleAns(azColName[i]);
		}
		
	}
	for (int i = 0; i < argc; i++)
	{
		// Inserting all others possible answers
		if (string(azColName[i]) != "ans")
		{
			question.addPossibleAns(argv[i]);
		}
	}

	((vector<Question>*)data)->push_back(question);

	return 0;
}

int callbacks::users(void* data, int argc, char** argv, char** azColName)
{

	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "name")
		{
			cout << "username: " << string(argv[i]) << ", ";
		}
		else if (string(azColName[i]) == "password")
		{
			cout << "password: " << string(argv[i]);
		}
	}
	cout << endl;
	return 0;
}
