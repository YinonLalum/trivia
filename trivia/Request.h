#pragma once

#include "Resurces.h"
#include "buffer.hpp"
#include "json.hpp"

typedef struct LoginRequest
{
	string username;
	string password;
} LoginRequest;

typedef struct SignupRequest
{
	string username;
	string password;
	string email;
} SignupRequest;

typedef struct GetPlayersInRoomRequest
{
	unsigned int roomId;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest
{
	unsigned int roomId;
} JoinRoomRequest;


typedef struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;


typedef struct SubmitAnswerRequest
{
	unsigned int answerId;
} SubmitAnswerRequest;


namespace JsonRequestPacketDeserializer
{
	unsigned int deserializeCode(buffer buffer);
	unsigned int deserializeLength(buffer buffer);
	LoginRequest deserializeLoginRequest(buffer buffer);
	SignupRequest deserializeSignupRequest(buffer buffer);
	GetPlayersInRoomRequest deserializeGetPlayersRequest(buffer buffer);
	JoinRoomRequest deserializeJoinRoomRequest(buffer buffer);
	CreateRoomRequest deserializeCreateRoomRequest(buffer buffer);
	SubmitAnswerRequest deserializeSubmitAnswerRequest(buffer buffer);
}

typedef enum requestID
{
	// Login handler
	login = 1,
	signup = 2,

	// Menu handler
	leave = 0,
	signout = 3,
	getRooms = 4,
	getPlayersInRoom = 5,
	joinRoom = 6,
	createRoom = 7,
	getHighScores = 8,
	getMyStatus = 9,

	// Room member request hndler
	leaveRoom = 10,
	getRoomState = 11,

	/// Room admin request handler
	closeRoom = 12,
	startGame = 13

} requestID;