#pragma once

#include "Question.h"
#include "Resurces.h"
#include "SqliteDataBase.h"
#include "Room.h"

typedef struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
}GameData;

class Game
{
private:
	vector<Question> _questions;
	map<LoggedUser, GameData> _players;

public:
	Game() = default;
	Game(Room room);
	Question getQuestionForUser(LoggedUser s);
	void submitAnswer(LoggedUser user, string answer);
	void removePlayer(LoggedUser user);
};