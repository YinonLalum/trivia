#include "Question.h"

Question::Question(string question, vector<string> possibleAnswers)
{
	_question = question;
	_possibleAnswers = possibleAnswers;
}

string Question::getQuestion()
{
	return _question;
}

vector<string> Question::getPossibleAnswers()
{
	return _possibleAnswers;
}

string Question::getCorrectAnswer()
{
	return _possibleAnswers[CORRRECT_ANSWER_INDEX];
}

void Question::addPossibleAns(string ans)
{
	_possibleAnswers.push_back(ans);
}
