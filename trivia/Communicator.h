#pragma once
#pragma comment (lib, "ws2_32.lib")

#include "Resurces.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <Windows.h>

#define CODE_SIZE 1
#define MSG_LENGTH_SIZE 4

class Communicator
{
private:
	SOCKET serverSocket;

	map<SOCKET, IRequestHandler*> _clients;
	RequestHandlerFactory* _handlerFactory;

	void startThreadForNewClient(SOCKET soc);
	void handleRequest(SOCKET soc);

public:
	Communicator(RequestHandlerFactory* handlerFactory);
	~Communicator();
	void bindAndListen(int port);

};