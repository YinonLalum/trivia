#pragma once

#include "Resurces.h"
#include "IDataBase.h"


class LoginManager
{
private:
	IDataBase* _databse;
	vector<LoggedUser> _loggedUsers;

public:
	LoginManager(IDataBase* dataBase);
	void signup(string name, string password, string email);
	bool login(string name, string password);
	void logout(LoggedUser user);
};
