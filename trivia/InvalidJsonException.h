#include "Resurces.h"


class InvalidJsonException : public std::exception
{
public:
	InvalidJsonException(const std::string& message) : std::exception(message.c_str()) {};
	InvalidJsonException() : std::exception("Error parsing the given Json") {};
};