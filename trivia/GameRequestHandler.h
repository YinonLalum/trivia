#pragma once

#include "IRequestHandler.h"
#include "GameManager.h"

class GameRequestHandler : public IRequestHandler
{
private:
	Game* _game;
	LoggedUser _user;
	GameManager* _gameManager;

private:
	RequestResult getQuestion(Request);
	RequestResult submitAnswer(Request);
	RequestResult getGameResults(Request);
	RequestResult leaveGame(Request);

public:
	GameRequestHandler(RequestHandlerFactory* fact, GameManager* mang, LoggedUser user);
	bool isRequestRelevant(Request);
	void suddenDC();
	RequestResult handleRequest(Request);

};
