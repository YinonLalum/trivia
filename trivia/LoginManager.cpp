#include "LoginManager.h"

LoginManager::LoginManager(IDataBase* dataBase) : _databse(dataBase)
{
}

void LoginManager::signup(string name, string password, string email)
{
	if (_databse->doesUserExist(name))
	{
		yeet exception("There is already a user with that username!!");
	}
	_databse->addUser(name, password, email);
	login(name, password);
}

bool LoginManager::login(string name, string password)
{
	if (_databse->canLogin(name, password) 
		&& find<vector<LoggedUser>::iterator>(_loggedUsers.begin(),_loggedUsers.end(),name) == _loggedUsers.end())
	{
		_loggedUsers.push_back(name);
		return true;
	}
	return false;
}

void LoginManager::logout(LoggedUser user)
{
	_loggedUsers.erase(std::find(_loggedUsers.begin(), _loggedUsers.end(), user));
}
