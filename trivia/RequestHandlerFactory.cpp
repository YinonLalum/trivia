#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#include "MenuRequestHandler.h"


RequestHandlerFactory::RequestHandlerFactory(IDataBase* dataBase): _loginManager(dataBase), _highscoreTable(dataBase), _gameManager(dataBase), _roomManager()
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this, &_loginManager);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return new MenuRequestHandler(this, &_roomManager, &_highscoreTable, &_loginManager, user);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, int roomId)
{
	return new RoomAdminRequestHandler(this, &_roomManager, user, roomId);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, int roomId)
{
	return new RoomMemberRequestHandler(this, &_roomManager, user, roomId);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser user)
{
	return new GameRequestHandler(this, &_gameManager, user);
}
